# Argumentaire général

Par: Julien Vaubourg, Luc Didry, Émile Morel, Christophe Masutti

## Internet et régulation

Il y a y presque cinquante ans, le concept de réseau émergeait dans le petit monde de l'informatique.
Il s'agissait de faire communiquer entre eux des terminaux distants, ce qui supposait
la mise en oeuvre de systèmes à temps partagé permettant la connexion simultanée de plusieurs 
utilisateurs sur une même machine «&nbsp;relais&nbsp;», qu'on appellera plus tard «&nbsp;serveur&nbsp;».
Avec le temps et la multiplication des services, de nouveaux usages de l'informatique en réseau ont 
entraîné l'apparition de multiples besoins de régulation. Le premier d'entre eux consistait à gérer 
l'interruption des programmes de manière à ce que tous les utilisateurs puissent partager le temps de 
calcul et les ressources physiques disponibles de la machine. Cela impliqua aussitôt des rapports 
hiérarchiques entre les utilisateurs&nbsp;: qui avait un accès à la machine avait un accès aux ressources. 
Et bien vite ce furent d'autres enjeux qui se présentèrent, comme la question des droits d'auteurs pour des 
fichiers  dupliqués et partagés, la resposnabilité des hébergeurs de services, les questions de censures, et bien 
entendu, présente depuis le début, l'analyse des risques sécuritaires.

La naissance d'Internet sortait ces rapport hiérarchiques du strict cadre des relations interpersonnelles entre 
collègues travaillant sur une seule machine. La régulation devait désormais reposer sur la nature des échanges. 
L'ensemble des utilisateurs opta alors pour la mise en oeuvre de protocoles d'échanges ouverts, c'est à dire 
non seulement capables de répondre à la plupart des besoins (courriers électroniques, pages web, 
stockage de fichiers, etc.)  mais aussi organisés de manière à ce que les utilisateurs puissent 
les améliorer et les adapter au fur et à mesure de l'évolution des usages. Cette régulation collective 
du réseau faisait en sorte que la diversité des types d'échanges possibles n'était plus limitée par 
des contraintes techniques mais uniquement par l'ingéniosité des contributeurs. En d'autres termes, 
il était impossible, pour une entreprise en particulier, d'établir un monopole sur l'échange 
de données en proposant un protocole à l'exclusion d'un autre. Internet fut construit sur ces bases 
faites de diversité et d'ouverture. Il imposait que le transfert de données devait passer par un ensemble 
de protocoles neutres, c'est à dire sans privilégier un type de contenu plutôt qu'un autre.

Lorsque le grand public eu enfin accès à ce réseau si prometteur (après un intense débat visant à 
déterminer si la génération de profits serait ou non autorisée sur Internet), une première régulation 
de fait s'imposa, celle des fournisseurs d'accès. Diverses tentatives eurent lieu, à commencer par la 
volonté de rendre captifs les utilisateurs dans une bulle génératrice de profits commerciaux et de 
consommation, ce qui fut le cas par exemple avec les premiers abonnements de la firme AOL. De manière 
générale, la première moitié des années 1990 fut un moment de maturation, marqué par l'explosion de 
la «&nbsp;bulle&nbsp;» Internet, suivie par une période de stabilisation des offres des fournisseurs 
d'accès et l'apparition des grands services centralisés des firmes comme Google, Amazon, Facebook, etc.

S'ils étaient pour la plupart présents dès les premiers balbutiements d'Internet, les principes de sa 
régulation se regroupent aujourd'hui en 5 grandes catégories sans aucune étanchéité entre elles&nbsp;:

   * La sécurité des données et des échanges. L'enjeu est double&nbsp;: il concerne la vie 
   privée des utilisateurs comme les questions de sécurité nationale (espionnages militaires et 
   industriels, risques terroristes, etc.),
   * Les politiques de gestion des flux et l'accroissement de la demande en bande passante. 
   Cela concerne par exemple la question du passage à IPv6.
   * Les modèles économiques acceptés et eux-mêmes régulés par les modèles juridiques qui s'y appliquent.
   * Les équilibres économiques des activités liées aux partages d'informations, et tout particulièrement 
   la question des licences et du droit d'auteur.
   * Les aspects démocratiques liés aux enjeux de la liberté d'expression sur Internet.

## Une affaire de contre-pouvoir

Tous ces enjeux ont mobilisé depuis longtemps l'attention des utilisateurs d'Internet les plus avertis. Cependant, 
le nombre d'utilisateurs allant croissant, il alimente de fait  les monopoles économiques et techniques 
des «&nbsp;géants&nbsp;» du web qui centralisent les services et les données au détriment des libertés des 
utilisateurs et des principes d'ouverture qui caractérisent Internet.

Que faire lorsque les plus grandes capitalisations financières du monde détiennent un 
monopole sur nos services de communication&nbsp;? Comment maintenir un acceptable degré 
d'information des utilisateurs sur les risques liés à la sécurité et à la vie privée&nbsp;? 
Les révélations d'E. Snowden n'ont fait que cristalliser, à un moment donné, les enjeux 
de pouvoir qui opposent d'un côté les citoyens et, de l'autre, l'étrange jeu de dupes 
entre la gouvernance du risque et les monopoles économiques.

L'une des réponses consiste à promouvoir des usages conformes aux principes d'ouverture, 
à l'encontre de la centralisation des services et des données. Ces usages passent 
nécessairement par une phase d'apprentissage technique, qu'il s'agisse de logiciels 
ou de matériels. L'essaimage des logiciels libres chez les utilisateurs constitue un 
premier pas décisif pour l'emploi de solutions alternatives permettant de rendre à la 
société civile (utilisateurs et concepteurs) la capacité de maîtriser ses échanges. 
Des  hébergeurs associatifs et même des fournisseurs d'accès à Internet associatifs, 
ont montré qu'une voie alternative peut reposer sur la confiance et la collaboration. 
Plus récemment, des matériels ont fait leur apparition sur le marché, permettant souvent 
pour un prix très modique, et moyennant l'usage de logiciels libres, d'auto-héberger ses 
propres données chez soi ou pour sa propre structure. De la même manière, des société de 
services en logiciels libres (SSLL) font leur apparition et proposent un nouveau marché 
plus responsable de l'hébergement de données envers le public et les entreprises.

En somme, l'indépendance des utilisateurs, si elle reste possible, dépend pour 
beaucoup des compétences techniques qu'il est aujourd'hui capital de partager. 
Elle dépend aussi de l'innovation dans le domaine des solutions d'auto-hébergement, 
censées rendre plus facile et efficace l'appropriation de ces solutions.

## Un ouvrage qui formalise les enjeux

De nombreuses ressources existent un peu partout sur le web, sous forme de sites
dédiés, du tutoriels issus de blogs personnels, etc. Elles participent à un immense
partage de connaissances et répondent au besoin de ré-appropriation d'Internet.
Or, autant les solutions n'ont de limites que les imaginations créatives des hackers,
autant la formalisation des questions n'a pas été clairement énoncée. Tout se passe
comme si les révélations de Snowden (et autrefois les alertes lancées à maintes reprises
par les passionnés et les activistes) avaient caché les détails et ce qui pourrait faire
office de plan d'action structuré. En d'autres termes, il existe aux yeux du public une désincarnation
des problématiques liées au respect des libertés des utilisateurs&nbsp;: en quoi l'ouverture des protocoles
de communication est importante&nbsp;? Pourquoi le chiffrement et les logiciels libres garantissent davantage
des pratiques respectueuses de mes données&nbsp;? Dans quelle mesure les solutions que je choisis pour
héberger mes données sur Internet sont déterminantes pour son avenir et le respect de ma vie privée&nbsp;?
Comment le tissu associatif et les entreprises du logiciel libre peuvent-ils générer, dans la société civile,
une forme de souveraineté numérique collective, solidaire, et avec quelles solutions&nbsp;? 

C'est à ces questions que l'ouvrage prétend répondre en trois parties. L'objectif sera d'abord de raconter
l'évolution du réseau Internet, pour comprendre pourquoi il s'est transformé de façon à ce qu'il soit
de plus en plus concentré autour de quelques acteurs commerciaux. Cette première partie représente le volet
politique, destiné à faire comprendre pourquoi la décentralisation est un enjeu majeur dans la lutte pour
le respect de notre vie privée (en n'oubliant pas de rappeler pourquoi le droit à la vie privée est aussi
important dans notre société). L'objectif est autant de convaincre ceux qui auraient quelques doutes sur
les risques de la centralisation des données et de leur surveillance permanente, que de leur apporter des
arguments clés pour pouvoir débattre plus facilement en famille, avec des amis ou au travail (e.g. pourquoi
la réponse « Je n'ai rien à cacher » n'est pas valable). Considérant que comprendre c'est déjà résister,
la seconde partie se concentrera sur la théorie des technologies de l'Internet, en y incluant GNU/Linux,
la sécurité, le routage, le mail, etc. Enfin, une dernière partie s'attardera sur les solutions pour échapper
à la surveillance de masse en hébergeant intelligemment ses données. Les deux principaux types de solutions
à présenter sont les solutions d'auto-hébergement clé en main (e.g. La Brique Internet), et les services
d'hébergement en ligne « éthiques » qui existent.

Voici quelques questions (non exhaustives) que le public du livre pourrait se poser&nbsp;:

   * Comment ça marche, tout cela&nbsp;? la seconde partie de l'ouvrage parlera des protocoles&nbsp;: 
   il s'agira d'expliquer en quoi leur emploi «&nbsp;fabrique&nbsp;» Internet. que peut-on dire aujourd'hui 
   des protocoles de communication qui ont bâti historiqueemnt Internet&nbsp;? ne 
   sont-ils pas finalement une manière d'incarner la démocratie dans la technique, et par
   conséquent dans quelle mesure la démocratie dépend-elle de leur ouverture et de
   leur emploi&nbsp;? (cf. le débat ipv4/ipv6, le chiffrement&hellip;)
   * aspects juridiques de l'hébergement de données&nbsp;: une solution éthique (comme par exemple le 
   principe du zero-knowledge) est-elle tenable juridiquement par rapport au dispositions légale de 
   le régulation ? Si j'héberge les données de mon voisin, en quoi suis-je responsable de quoi&nbsp;?
   * le matériel (la Brique, Raspberry, etc.) et ses modèles économiques reposant sur l'ouverture ou non 
   des techniques employées&nbsp;: dans quelle mesure ce matériel est censé modeler 
   un Internet «&nbsp;différent&nbsp;»&nbsp;? 
   * le grand principe de la souveraineté numérique est souvent conçu par le gouvernement comme l'incitation 
   à des entreprises du territoire de développer des modèles concurrentiels aux géants du web. Finalement, est-ce 
   ce que la solution n'est pas plutôt du côté des utilisateurs eux-mêmes qui devraient développer le réseau à 
   partir de leurs initiatives tout en s'appuyant sur des structures (public, privée, ou associatives) qui proposent 
   des solutions techniques. En somme, est-ce que la souveraineté numérique ne devrait pas être une appropriation 
   d'Internet par et pour les citoyens&nbsp;? (principes du logiciel libre appliqué au réseau en général)
   * en quoi le logiciel libre peut-il est porteur d'un modèle de développement d'un Internet «&nbsp;éthique&nbsp;»&nbsp;? 
   s'il repose sur le bénévolat, sur l'implication de quelques individus, ce modèle n'est-il amené à s'essouffler&nbsp;?

## Public visé

   * Qui voudrait s'auto-héberger ou cherche une solution tierce, devrait lire l'ouvrage auparavant
   (en particulier avant de chercher un tutoriel sur le web).
   * Acteurs associatifs, journalistes, militants.
   * Responsables de structures (grandes ou petites, privées ou non).
   * Des Dupuys-Morizeau débutants mais un minimum intéressés par l'informatique.


