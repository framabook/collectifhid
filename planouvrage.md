# Plan en cours d'élaboration

Titre : *Héberger intelligemment ses données*

Comité de direction : Julien Vaubourg, Luc Didry, Émile Morel, Christophe Masutti

Collection : Framabook (Framasoft)

---

**Le titre est temporaire**, il faudra trouver quelque chose de plus *catchy* et compréhensible pour le grand public.

Le **public visé correspond aux gens qui ont été interpellés** par les révélations de Snowden, les articles
d'alerte sur la Loi Renseignement, ou qui entament eux-même une réflexion à propos du pouvoir de Google et cie.

La partie *Politique* est destinée à **les aider dans leur réflexion**, et leur donner les bons arguments à ressortir
en famille, avec leurs amis ou au travail.

La partie *Théorie* est destinée à leur **faire comprendre les fondamentaux d'Internet**, pour mieux comprendre les
enjeux et les contraintes, en retirant la part de «&nbsp;magie&nbsp;» qui les desservent au quotidien (comprendre
c'est déjà résister).

La partie *Solutions* correspond **à la fois aux gens qui souhaitent mettre un minimum les mains dans le cambouis**
avec des solutions d'auto-hébergement *clé en main* et **ceux qui ne souhaitent pas y consacrer particulièrement
de temps** et qui ont besoin de services en ligne prêt-à-utiliser qui soient *éthiques*.

Le **plan présenté ci-dessous est également temporaire** et pourra être modifié au fur et à mesure de l'avancement du
livre, des contributions des auteurs et des réflexions qui vont graviter autour.

## Partie 1: Politique (les principes)

Intro : comprendre les enjeux d'Internet au regard se sa régulation et des 
principes d'ouverture sur lesquels il repose

   1. Comment Internet a été pensé
      * Proposition : Valentin Lacambre (co-fondateur Gandi)
      * Contenu : Internet était un truc cool à la base (e.g. La Déclaration d'indépendance du cyber-space)
   2. Centralisation vs. décentralisation
      * Proposition : Benjamin Bayart (président FFDN)
      * Contenu : En gros Minitel 2.0
   3. Neutralité du Net
      * Proposition : Adrienne Charmet (porte-parole LQDN)
      * Contenu : La neutralité du Net
   4. Surveillance de masse / Rien à cacher
      * Proposition : Ohkin / Kheops (militants Telecomix)
      * Contenu : Wikileaks, Snowden, NSA, Loi Renseignement
   5. Les logiciels libres
      * Proposition : Hugo Roy (juriste militant du LL membre FSFE)
      * Contenu : Peut-on vraiment faire de l'auto-hébergement sans LL ?

## Partie 2 : Théorie (logiciels, protocoles et pratiques)

intro : comment s'organise Internet, avec quelles solutions logicielles et comment s'approprier ses protocoles ? 

   1. GNU/Linux et le projet Debian
      * Proposition : Lucas Nussbaum (ancien responsable du projet Debian)
      * Contenu : C'est quoi GNU/Linux, aspect communautaire de Debian
   2. Le réseau Internet
      * Proposition : Stéphane Bortzmeyer (guru) / Laurent Guerby (co-fondateur Tetaneutral.net)
      * Contenu : C'est quoi une IP (v6/v4), c'est quoi le routage, DNS, protocoles, ports... version light
   3. Sécurité
      * Proposition : Lunar (co-fondateur NosOignons) / Benjamin Sonntag (co-fondateur LQDN)
      * Contenu : Clés symétriques, asymétriques, chiffrement au niveau transport et données avec e.g. HTTPS, SSH, GPG, OTR
   4. Le mail
      * Proposition : Benjamin Sonntag (co-fondateur LQDN)
      * Contenu : comment ça marche, MX/SMTP/IMAP/POP/SPF/DKIM, résilience
   5. La loi
      * Proposition : Benjamin Jean (Framasoft, Inno3)
      * contenu : quelles responsabilités dans l'auto-hebergement (associatif, privé) ?

## Partie 3 : Solutions (associations, sociétés de services, solidarité)

intro: c'est bien beau tout cela, mais quelles solutions à portée de la société civile ?

   1. Préambule
      * Proposition : À proposer
      * Contenu : gérer un nom de domaine / gérer ses backups / les RBL
   2. Auto-hébergement facile
      * Choisir son FAI
         * Proposition : À proposer
         * Contenu : assos FFDN, configuration de box, limitations FAI français
      * YunoHost / La Brique Internet
         * Proposition : kload (co-fondateur YunoHost) / Julien Vaubourg (co-fondateur La Brique Internet)
      * CozyCloud
         * Proposition : Tristan Nitot
   3. Services mutualisés de confiance
      * Les assos de la FFDN / CHATONS
         * Proposition : À proposer
         * Contenu : comment les trouver, ce qu'ils garantissent et ce qu'ils font
      * Services Framasoft
         * Proposition : quelqu'un de chez Framasoft
      * La Mère Zaclys
         * Proposition : Framasoft a des contacts
      * IndieHosters
         * Proposition : Pierre Ozoux
      * Outils Riseup
         * Proposition : Quelqu'un de Riseup
      * Toile Libre / Mailoo
         * Proposition : Quelqu'un de TL / Mailoo
      * D'autres ?
   4. Solutions commerciales (à discuter)
      * Mails chez OVH
         * Proposition : À proposer
      * Mails chez Gandi
         * Proposition : À proposer
