# Consignes aux auteurs

La longueur des chapitres de l'ouvrage est libre. Il faudra cependant veiller à l'équilibre des parties.

Dans sa version papier, l'ouvrage sera au format A5, noir et blanc, couverture couleur.

En contribuant à cet ouvrage collectif, vous acceptez de placer votre production (chapitre) sous une licence libre, comme par exemple : CC-By-Sa, CC-By, GNU FDL, Licence Art Libre, ou combinaisons éventuelles, ou autre licence compatible.

Tous les éléments dont vous vous servez (notamment les illustrations) devront être placés sous une licence libre compatible avec celles mentionnées ci-dessus. Vous n'oublierez pas de signaler les sources et les paternités correspondantes.

## Méthodologie pour l'avancement de l'ouvrage

Plusieurs étapes seront nécessaires pour l'avancement collectif de l'ouvrage&nbsp;:

   0. les auteurs seront invités, une fois leur déclaration d'intention faite, pour s'inscrire à une liste de discussion sur [Framalistes](http://framalistes.org/sympa/info/collectifhid)&nbsp;;
   1. un dépôt Git est créé pour les besoins de l'ouvrage à l'adresse [git.framasoft.org/framabook/collectifhid](https://git.framasoft.org/framabook/collectifhid). Signalez-vous sur la liste pour être membre du projet et y contribuer&nbsp;;
   2. Les auteurs livreront leurs chapitres en version finale sur ce dépôt ou, s'ils ne sont pas à l'aise avec Git, sur la liste de discussion ou directement à C. Masutti&nbsp;;
   3. Les auteurs seront conviés à se relire entre eux, non pas pour effectuer des corrections (rôle du groupe Framabook), mais pour éventuellement apporter des modifications à leurs propres textes, en vue d'augmenter la cohésion de l'ouvrage (des discussions pourront avoir lieu sur la liste de discussion)&nbsp;;
   4. Phase éditoriale Framabook : les auteurs pourront éventuellement être contactés sur des points précis de leurs textes&nbsp;;
   5. Phase de publication&nbsp;: sortie de l'ouvrage dans la collection Framabook, communiqués de presse, etc.

*Note* : une discussion aura lieu au sujet de la vente de la version papier de l'ouvrage et de son prix. Le principe de la collection Framabook sera néanmoins respecté : chaîne éditoriale, publication, vente au format papier et disponibilité gratuite des versions électroniques.


  

## Livraison des chapitres

Les chapitres seront livrés **au format [markdown](http://daringfireball.net/projects/markdown/)**, très simple à la saisie et qui permettra diverses exportations, notamment en vue d'une mise en page avec LaTeX et HTML.

Si toutefois vous ne parvenez pas à utiliser ce format, vous pourrez toujours livrer votre chapitre au format ODT, à condition d'utiliser uniquement les formats et styles par défaut (nous convertirons alors en markdown).

Même si la tentation est grande, merci de ne pas livrer vos textes au format TeX. En effet, nous devons partir du format markdown pour diviser ensuite la branche du projet en deux parties : Tex et HTML.

Framabook utilisera un logiciel pour faire diverses conversions: **Pandoc**. Ce dernier comporte plusieurs extentions qui changent quelque peu la saveur du markdown employé. La syntaxe markdown est expliquée sur [site officiel](http://daringfireball.net/projects/markdown/). Les consignes suivantes donnent une idée de la saveur Pandoc.

## Commentaires

Vous pouvez commenter votre texte, notamment à l'intention des relecteurs, en insérant des commentaires qui ne seront pas interprétés, de cette manière&nbsp;:

	<!-- ceci est un commentaire -->


## Notes de bas de page

Les notes de bas de page devront se présenter comme suit&nbsp;: ``[^mareference1]`` et à la fin du document sont rassemblées les notes, si possible dans l'ordre, selon cette syntaxe exactement&nbsp;:

    [mareference1]: Ceci est la note.

Où ``mareference`` est une chaîne de caractère que l'auteur choisira pour tout son texte, par exemple ``dupontnote1``, ``dupontnote2``, etc. de manière à différencier ses notes de celles des autres auteurs (à l'heure actuelle, nous ne savons pas encore si les notes seront rassemblées en fin de chapitre, en fin de partie ou en fin de volume).


## Références

Limitez le nombre de références dans votre texte&nbsp;: une référence n'est utile que pour le lecteur qui souhaite approfondir le propos que vous lui tenez. Il sera donc inutile de faire un lien vers la notice wikipédia de chaque concept que vous utilisez. Mieux encore&nbsp;: définissez vous-même les concepts que vous uilisez. Nous n'élaborons pas ici un ouvrage académique, les règles de citation sont donc plus légères. Les références bibliographiques et liens seront utilisés essentiellement pour&nbsp;:

   * rendre à César&hellip; c'est à dire attribuer la paternité d'une thèse ou d'un argumentaire à quelqu'un d'autre que soi,
   * proposer au lecteur d'approfondir un sujet (sous-entendu&nbsp;: ne pas le noyer dans un trop grand nombre de références, au risque de le perdre définitivement),
   * développer un aspect technique ou scientifique que vous n'avez pas le temps d'expliquer (c'est le genre de référence que l'on retrouve en fin d'une note de bas de page qui résume le propos en aparté et renvoie le lecteur vers une autre source).

### Références bibliographiques

Les auteurs devront fournir leurs références bibliographiques au format ``.bib``. Ils peuvent pour cela utiliser un logiciel comme [Zotero](https://www.zotero.org/) (une extension à Firefox, mais qui peut s'utiliser en standalone), qui permet un export en BibTex, ou encore le logiciel [JabRef](http://jabref.sourceforge.net/). Avec ce format, une clé correspond à chaque référence bibliographique. Elle sera utilisée dans le texte sous la forme ``[@Shannon1982]`` ou ``[@Schannon1982;@Stallman1987...]``.

### Liens

Les liens seront ajoutés selon la syntaxe markdown. Ils apparaîtront, dans la version papier (PDFLaTeX) de l'ouvrage en note de bas de page. Pour des raisons esthétiques dans la mise en page papier, essayez de limiter la longueur des liens. Par exemple, au lieu de donner l'adresse ``http://site.domaine.org/fr/notices/poisson?rechercheKGL?requin&ocean/``, écrivez plutôt (en markdown)&nbsp;:

    Voyez sur [domaine.org](http://site.domaine.org/) la notice « requin ».


## Les illustrations : figures et tableaux

Dans le texte, toutes les figures (images, shéma&hellip;) et les tableaux devront être accompagnées d'une légende qui explique en quoi ils illustrent le propos. 

Concernant les tableaux, il faudra limiter leur nombre. Merci de les éviter dans la mesure du possible (difficile à gérer pour les diverses conversions). En cas d'absolue nécessité de fournir un tableau (si vous risquez l'apoplexie en cas de privation) faire des tableaux très simples avec peu de texte dans les cases, ou bien les fournir en svg (cf. ci-dessous).

### Images matricielles

Les images matricielles devront être fournies au format PNG ou JPG) dans une résolution de 300&nbsp;dpi et en nuances de gris. Attention aux contrastes : ils devront être bien nets en vue de l'impression et comporter des nuances de gris pas trop fines.

### Format vectoriel

Chaque fois que cela est possible, les auteurs sont invités à fournir des illustrations au format vectoriel (SVG, si possible réalisé avec le logiciel Inkscape). Dans ce dernier, cas, une version matricielle de faible résolution devra aussi être fournie pour être certain du rendu final, de la volonté de l'auteur et des possibilités de modifications pour les besoins de la mise en page.

### Insertion des images

Markdown permet l'usage de balises en-ligne pour les images, avec la syntaxe suivante&nbsp;:

     ![Texte alternatif](/chemin/vers/image)

Le ``Texte alternatif`` est la description utilisée en HTML. Vous y mettrez la légende complète de l'image. Si d'autres informations sont nécessaires, merci de les placer en commentaire.



## Typographie

De manière générale, le groupe Framabook s'occupera des détails typographiques. Néanmoins, quelques explications sont nécessaires.

Merci de ne pas inclure d'espaces insécables. Nous nous en occuperons. Les auteurs se contenteront d'insérer des espaces normaux à ces endroits&nbsp;:

   * après les guillemets ouvrant et avant les guillemets fermant,
   * avant une ponctuation double (double-point, point d'interrogation, point d'exclamation, point-virgule),
   * avant et après un tiret d'incise (celui-ci devra être soit un demi-quadratin soit deux traits d'union (``--``)), sauf si l'incise se termine par une ponctuation finale.

### Citations

Une citation à l'intérieur d'un paragraphe est écrite entre guillemets français (``«`` et ``»``). Le point final d'une citation comprise à l'intérieur d'un paragraphe se place *après* le guillemet fermant *sauf* si la citation termine le paragraphe, auquel cas, le point final est placé *avant* le guillemet fermant.

Les citations longues sont placées en exergue et ne comportent pas de guillemets. Selon la syntaxe markdown, les lignes des citations commencent par ``>``. Voici un exemple&nbsp;:

	> Voici venu le temps des rires et des chants,
	dans l'île aux enfants, c'est tous les jours
	le printemps.
	> 
	> C'est le pays joyeux des enfants heureux,
	des monstres gentils, oui, c'est le paradis !

Ce qui produit&nbsp;:

> Voici venu le temps des rires et des chants, dans l'île aux enfants, c'est tous les jours le printemps.
> 
> C'est le pays joyeux des enfants heureux, des monstres gentils, oui, c'est le paradis&nbsp;!

### Effets de caractères

Le gras est strictement interdit, il est réservé aux titres.

L'italique (ou l'emphase) est utilisé exclusivement pour les titres d'ouvrage (les titres d'articles sont entre guillemets), les mots ou expressions en langue étrangère (ou morte). De manière très anecdotique, un concept ou une notion peuvent être mis en italique, à condition de le justifier.

### Bonnes pratiques

De manière générale, concernant la typographie, fiez vous au simple bon sens. Votre texte sera relu et corrigé quoi qu'il en soit. Merci de faciliter le travail des correcteurs. Ces derniers se réfèrent, en cas de doute, au *Lexique des règles typographiques en usage à l'Imprimerie Nationale*. La lecture de cet ouvrage est conseillée pour occuper vos longues soirées d'hiver.
